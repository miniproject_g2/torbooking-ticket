/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package booking;

/**
 *
 * @author Nobpharat
 */
public class History {
    private String[][][] history;
    private int numBookings;

    public History() {
        int maxBookings = 100;
        history = new String[maxBookings][][];
        numBookings = 0;
    }

    public void addBooking(String movieTitle, String showTime, int movieDuration, String[] bookedSeats, double totalPrice, Seat[][] seatMap) {
        String[][] booking = new String[bookedSeats.length + 5][2];
        booking[0][0] = "Movie:";
        booking[0][1] = movieTitle;
        booking[1][0] = "Show Time:";
        booking[1][1] = showTime;
        booking[2][0] = "Movie Duration:";
        booking[2][1] = movieDuration + " minutes";
        booking[3][0] = "Seats Booked:";
        booking[3][1] = String.join(", ", bookedSeats);
        for (int i = 0; i < bookedSeats.length; i++) {
            booking[i + 4][0] = "Seat " + (i + 1);
            booking[i + 4][1] = bookedSeats[i];
        }
        booking[bookedSeats.length + 4][0] = "Seat Map:";
        StringBuilder seatMapString = new StringBuilder();
        for (Seat[] row : seatMap) {
            for (Seat seat : row) {
                seatMapString.append(seat.isAvailable() ? seat.toString() + " " : seat + " ");
            }
            seatMapString.append("\n");
        }
        booking[bookedSeats.length + 4][1] = seatMapString.toString();
        history[numBookings] = booking;
        numBookings++;
    }

    public void printHistory() {
        if (numBookings == 0) {
            System.out.println("No booking history available.");
            return;
        }
        System.out.println("Booking History:");
        for (int i = 0; i < numBookings; i++) {
            System.out.println("Booking " + (i + 1) + ":");
            for (String[] entry : history[i]) {
                System.out.println(entry[0] + " " + entry[1]);
            }
            System.out.println();
        }
    }
}