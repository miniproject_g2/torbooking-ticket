/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package booking;

/**
 *
 * @author Nobpharat
 */
public class Seat {
    private char row;
    private int column;
    private double price;
    private boolean isAvailable;

    public Seat(char row, int column, double price) {
        this.row = row;
        this.column = column;
        this.price = price;
        this.isAvailable = true;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void book() {
        isAvailable = false;
    }

    public void cancel() {
        isAvailable = true;
    }

    public double getPrice() {
        return price;
    }

    public String toString() {
        return row + "" + column;
    }
}
