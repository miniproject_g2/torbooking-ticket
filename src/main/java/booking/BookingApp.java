package booking;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class BookingApp {

    static int selectedShowTime;
    static int timeNow;

    public static double calculateTotalPrice(String[] bookedSeats, Theater theater, int selectedMovie) {
        double totalPrice = 0;
        for (String seatName : bookedSeats) {
            Seat seat = theater.getSeatByName(seatName, selectedMovie, selectedShowTime);
            if (seat != null) {
                totalPrice += seat.getPrice();
            }
        }
        return totalPrice;
    }

    public static int getValidMovieSelection(Scanner scanner, int numMovies) {
        System.out.print("Enter the number of the movie you want to watch: ");

        try {
            int selectedMovie = scanner.nextInt() - 1;
            scanner.nextLine();

            if (selectedMovie < 0 || selectedMovie >= numMovies) {
                System.out.println("Invalid movie selection. Please try again.");
                selectedMovie = getValidMovieSelection(scanner, numMovies);
            }
            return selectedMovie;
        } catch (Exception e) {
            scanner.nextLine();
            System.out.println("Invalid input. Please enter a valid option.");
            return getValidMovieSelection(scanner, numMovies);
        }
    }

    public static int getValidShowTimeSelection(Scanner scanner, String[] showTimes) {
        System.out.print("Select the show time by entering the number: ");

        try {
            selectedShowTime = scanner.nextInt() - 1;
            timeNow = selectedShowTime;
            scanner.nextLine(); // Consume newline

            if (selectedShowTime < 0 || selectedShowTime >= showTimes.length) {
                System.out.println("Invalid show time selection. Please try again.");
                selectedShowTime = getValidShowTimeSelection(scanner, showTimes);
            }
            return selectedShowTime;
        } catch (Exception e) {
            scanner.nextLine(); // Consume invalid input
            System.out.println("Invalid input. Please enter a valid option.");
            return getValidShowTimeSelection(scanner, showTimes);
        }
    }

  public static String[] bookSeats(Scanner scanner, Theater theater, int selectedMovie, String showTime) {
    System.out.println("Available Seats for " + theater.getMovieTitle(selectedMovie)
            + " at " + showTime + ":");
    theater.displayAvailableSeats(selectedMovie, selectedShowTime);

    System.out.println("How many seats would you like to book?");

    try {
        int numSeatsToBook = scanner.nextInt();
        scanner.nextLine(); 

        String[] bookedSeats = new String[numSeatsToBook];

        for (int i = 0; i < numSeatsToBook; i++) {
            System.out.print("Enter seat name to book (Ex. A1): ");
            String seatName = scanner.next();

            boolean booked = theater.bookSeatByName(seatName, selectedMovie, selectedShowTime);

            if (booked) {
                bookedSeats[i] = seatName;
                System.out.println("Seat " + seatName + " booked successfully!");
            } else {
                System.out.println("Seat " + seatName + " not available or invalid seat name.");
                i--;
            }
        }

        return bookedSeats;
    } catch (Exception e) {
        scanner.nextLine(); 
        System.out.println("Invalid input. Please enter a valid number.");
        return bookSeats(scanner, theater, selectedMovie, showTime);
    }
}




    public static void displayBookingDetails(Theater theater, int selectedMovie, String selectedShowTime,
            String[] bookedSeats, double totalPrice) {
        System.out.println("Booking Details:");
        System.out.println("Movie: " + theater.getMovieTitle(selectedMovie));
        System.out.println("Show Time: " + selectedShowTime + " (Duration: " + theater.getMovieDuration(selectedMovie) + " minutes)");
        System.out.println("Seats Booked:");
        for (String seat : bookedSeats) {
            System.out.println(seat);
        }
        System.out.println("Total Price: $" + totalPrice);
        System.out.println("Seats : " + theater.getMovieTitle(selectedMovie)
                + " at " + selectedShowTime + ":");
        theater.displayAvailableSeats(selectedMovie, timeNow);
    }

    public static boolean confirmBooking(Scanner scanner) {
        System.out.print("Do you want to confirm the booking? (y/n): ");
        String response = scanner.next();
        return response.equals("y");
    }

    public static void bookTicket(Scanner scanner, Theater theater, History history) {
        System.out.println("Available Movies:");
        for (int i = 0; i < theater.getNumMovies(); i++) {
            System.out.println((i + 1) + ". " + theater.getMovieTitle(i));
        }

        int selectedMovie = getValidMovieSelection(scanner, theater.getNumMovies());

        System.out.println("Available Show Times for " + theater.getMovieTitle(selectedMovie) + ":");
        String[] showTimes = theater.getMovieShowTimes(selectedMovie);
        for (int i = 0; i < showTimes.length; i++) {
            System.out.println((i + 1) + ". " + showTimes[i] + " (Duration: " + theater.getMovieDuration(selectedMovie) + " minutes)");
        }

        int selectedShowTime = getValidShowTimeSelection(scanner, showTimes);
        String selectedShowTimeString = showTimes[selectedShowTime];

        String[] bookedSeats = bookSeats(scanner, theater, selectedMovie, selectedShowTimeString);

        double totalPrice = calculateTotalPrice(bookedSeats, theater, selectedMovie);

        displayBookingDetails(theater, selectedMovie, selectedShowTimeString, bookedSeats, totalPrice);

        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedDateTime = currentDateTime.format(formatter);
        System.out.println("Booking time: " + formattedDateTime);

        boolean isConfirmed = confirmBooking(scanner);
        if (isConfirmed) {
            Seat[][] seatMap = theater.getSeatMap(selectedMovie, selectedShowTime);
            history.addBooking(theater.getMovieTitle(selectedMovie), selectedShowTimeString, theater.getMovieDuration(selectedMovie), bookedSeats, totalPrice, seatMap, formattedDateTime);
            System.out.println("Thank you for booking! Enjoy the movie!");
        } else {
            for (String seat : bookedSeats) {
                theater.cancelSeatByName(seat, selectedMovie, selectedShowTime);
            }
            System.out.println("Booking canceled. Seats have been released.");
        }
    }

    public static void main(String[] args) {
        int numRows = 6;
        int numColumns = 10;
        int numMovies = 3;
        int numShowTimes = 3;
        Theater theater = new Theater(numRows, numColumns, numMovies, numShowTimes);

        String[] time1 = {"10:00 AM", "1:00 PM", "4:00 PM"};
        String[] time2 = {"11:00 AM", "2:00 PM", "5:00 PM"};
        String[] time3 = {"12:00 PM", "3:00 PM", "6:00 PM"};

        theater.addMovie("Doraemon The Movie", 12.0, time1, 90, 0); // Duration: 1 hour and 30 minutes
        theater.addMovie("Mission Impossible 6", 10.0, time2, 120, 1); // Duration: 2 hours
        theater.addMovie("London Has Fallen", 8.0, time3, 150, 2); // Duration: 2 hours and 30 minutes

        Scanner scanner = new Scanner(System.in);

        boolean continueProgram = true;
        History history = new History();

        while (continueProgram) {
            System.out.println("1. Book a Ticket");
            System.out.println("2. Watch Booking History");
            System.out.println("3. Exit Program");
            System.out.print("Enter your choice: ");

            try {
                int choice = scanner.nextInt();
                scanner.nextLine();

                switch (choice) {
                    case 1:
                        bookTicket(scanner, theater, history);
                        break;
                    case 2:
                        history.printHistory();
                        break;
                    case 3:
                        continueProgram = false;
                        System.out.println("Exiting the program. Goodbye!");
                        break;
                    default:
                        System.out.println("Invalid choice. Please select again.");
                        break;
                }
            } catch (Exception e) {
                scanner.nextLine();
                System.out.println("Invalid input. Please enter a valid option.");
            }
        }

        scanner.close();
    }
}
