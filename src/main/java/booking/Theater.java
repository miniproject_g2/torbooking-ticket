/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package booking;

/**
 *
 * @author Nobpharat
 */
class Theater {
    private int numRows;
    private int numColumns;
    private int numMovies;
    private int numShowTimes;
    private Seat[][][][] movieSeats;
    private Movie[] movies;

    public Theater(int numRows, int numColumns, int numMovies, int numShowTimes) {
        this.numRows = numRows;
        this.numColumns = numColumns;
        this.numMovies = numMovies;
        this.numShowTimes = numShowTimes;
        movieSeats = new Seat[numMovies][numShowTimes][numRows][numColumns];
        initializeSeats();
        movies = new Movie[numMovies];
    }

    private void initializeSeats() {
        double cheapSeatPrice = 8.0;
        double regularSeatPrice = 12.0;

        for (int i = 0; i < numMovies; i++) {
            for (int j = 0; j < numShowTimes; j++) {
                for (int k = 0; k < numRows; k++) {
                    for (int l = 0; l < numColumns; l++) {
                        double seatPrice = (k <= 2) ? cheapSeatPrice : regularSeatPrice;
                        movieSeats[i][j][k][l] = new Seat((char) ('A' + k), l + 1, seatPrice);
                    }
                }
            }
        }
    }

    public void addMovie(String title, double price, String[] showTimes, int duration, int index) {
        movies[index] = new Movie(title, price, showTimes, duration);
    }

    public String getMovieTitle(int movieIndex) {
        return movies[movieIndex].getTitle();
    }

    public String[] getMovieShowTimes(int movieIndex) {
        return movies[movieIndex].getShowTimes();
    }

    public int getMovieDuration(int movieIndex) {
        return movies[movieIndex].getDuration();
    }

    public void displayAvailableSeats(int movieIndex, int showtimeIndex) {
        for (Seat[] row : movieSeats[movieIndex][showtimeIndex]) {
            for (Seat seat : row) {
                System.out.print(seat.isAvailable() ? seat + " " : "X ");
            }
            System.out.println();
        }
    }

    public Seat[][] getSeatMap(int movieIndex, int showtimeIndex) {
        return movieSeats[movieIndex][showtimeIndex];
    }

    public Seat getSeatByName(String seatName, int movieIndex, int showtimeIndex) {
        int row = seatName.charAt(0) - 'A';
        int column = Integer.parseInt(seatName.substring(1)) - 1;

        if (row >= 0 && row < numRows && column >= 0 && column < numColumns) {
            return movieSeats[movieIndex][showtimeIndex][row][column];
        }
        return null;
    }

    public boolean bookSeatByName(String seatName, int movieIndex, int showtimeIndex) {
        Seat seat = getSeatByName(seatName, movieIndex, showtimeIndex);
        if (seat != null && seat.isAvailable()) {
            seat.book();
            return true;
        }
        return false;
    }

    public int getNumMovies() {
        return movies.length;
    }

    public boolean cancelSeatByName(String seatName, int movieIndex, int showtimeIndex) {
        Seat seat = getSeatByName(seatName, movieIndex, showtimeIndex);
        if (seat != null && !seat.isAvailable()) {
            seat.cancel();
            return true;
        }
        return false;
    }
}
