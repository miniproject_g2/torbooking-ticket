/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package booking;

/**
 *
 * @author Nobpharat
 */
public class Movie {
    private String title;
    private double price;
    private String[] showTimes;
    private int duration; // duration in minutes

    public Movie(String title, double price, String[] showTimes, int duration) {
        this.title = title;
        this.price = price;
        this.showTimes = showTimes;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public double getPrice() {
        return price;
    }

    public String[] getShowTimes() {
        return showTimes;
    }

    public int getDuration() {
        return duration;
    }
}

